Funcionalidade: Remover participante do grupo
	Funcionalidade destinada a testar a remoção de um integrante do grupo
	Testes devem ser realizados no IOS.

Cenario: CEN02-TC01-Remover um integrante do grupo
	Dado Que estou com o whatsapp aberto na tela de conversas, clicar em cima do nome do grupo
	Quando Estiver na tela de mensagem do grupo, clicar no nome do grupo no topo da tela
	E Clicar em cima do nome do participante
	E Clicar no item REMOVER (integrante)
	E Será exibido um modal pergutando se deseja remover o integrante. Clicar em REMOVER
	Entao Verificar na lista de participantes se o integrante consta na lista. Não deve constar.